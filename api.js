const randomColor = require('randomcolor');
const faker = require('faker');
const _ = require('lodash');


module.exports = () => {
  const data = {};

  data.users = _.times(10, (n) => {
    return {
      id: n + 1,
      _id: n + 1,
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      login: faker.internet.userName()
    };
  });

  data.projects = _.times(10, (n) => {
    return {
      id: n + 1,
      _id: n + 1,
      title: faker.name.title(),
      color: randomColor()
    };
  });

  data.emails = _.times(100, (n) => {
    return {
      id: n + 1,
      _id: n + 1,
      userId: _.random(1, 10),
      projectId: _.random(1, 10),
      from: [{name: faker.name.findName(), address: faker.internet.email()}],
      to: [{name: faker.name.findName(), address: faker.internet.email()}, {name: faker.name.findName(), address: faker.internet.email()}],
      cc: [{name: faker.name.findName(), address: faker.internet.email()}],
      subject: faker.name.title(),
      html: faker.lorem.paragraphs(),
      text: faker.lorem.sentences(),
      date: new Date(),
      isRead: faker.random.boolean(),
      isDeleted: faker.random.boolean(),
      created: faker.date.recent()
    };
  });

  return data;
};
