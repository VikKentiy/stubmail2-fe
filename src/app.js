import 'bootstrap/less/bootstrap.less';
import 'font-awesome/scss/font-awesome.scss';

import './assets/scss/fonts.scss';
import './assets/css/style.css';

import angular from 'angular';
import uiRouter from 'angular-ui-router';

import Common from './common';
import Components from './components';
import {
  AppConfig
} from './app.config';
import {
  AppComponent
} from './app.components';

angular
  .module('app', [Common, Components, uiRouter])
  .directive('app', AppComponent)
  .config(AppConfig);
