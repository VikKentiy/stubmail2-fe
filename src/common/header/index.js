import angular from 'angular';
import component from './header.component';

export default angular
  .module('app.header', [])
  .directive('header', component)
  .name;
