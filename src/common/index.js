import angular from 'angular';
import Header from './header';

export default angular
  .module('app.common.components', [Header])
  .name;
