import template from './footer.jade';

const component = () => {
  return {
    template,
    restrict: 'E',
    replace: false
  };
};

export default component;
