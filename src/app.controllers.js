export class AppController {
  constructor($rootScope, AuthService) {
    'ngInject';
    AuthService.getCurrentUser().then(user => {
      $rootScope.user = user;
    });
  }
}
