import template from './app.jade';
import {
  AppController
} from './app.controllers';

export const AppComponent = () => {
  return {
    controller: AppController,
    template: template,
    restrict: 'E'
  };
};
