import MailBoxTemplate from './templates/mailbox.jade';
import MailBoxListTemplate from './templates/mailbox-list.jade';
import MailBoxDetailTemplate from './templates/mailbox-detail.jade';
import {
  MailboxController,
  MailboxListController,
  MailboxDetailController
} from './mailbox.controllers';

/* @ngInject*/
export const MailBoxComponent = () => {
  return {
    controller: MailboxController,
    controllerAs: 'mpc',
    template: MailBoxTemplate,
    restrict: 'E',
    replace: true
  };
};

/* @ngInject*/
export const MailBoxListComponent = () => {
  return {
    controller: MailboxListController,
    controllerAs: 'mlc',
    template: MailBoxListTemplate,
    restrict: 'E',
    scope: {
      type: '@'
    }
  };
};

/* @ngInject*/
export const MailBoxDetailComponent = () => {
  return {
    controller: MailboxDetailController,
    controllerAs: 'mdc',
    template: MailBoxDetailTemplate,
    restrict: 'E'
  };
};
