import angular from 'angular';
import 'checklist-model';
import 'angular-moment';

import {
  MailBoxComponent,
  MailBoxListComponent,
  MailBoxDetailComponent
} from './mailbox.components';
import {
  MailboxProject,
  MailboxEmail
} from './mailbox.services';
import {
  MailboxConfig
} from './mailbox.config';
import './mailbox.scss';

export default angular
  .module('app.mailbox', ['ui.router', 'checklist-model', 'angularMoment'])
  .service('MailboxEmail', MailboxEmail)
  .service('MailboxProject', MailboxProject)
  .directive('mailbox', MailBoxComponent)
  .directive('mailboxList', MailBoxListComponent)
  .directive('mailboxDetail', MailBoxDetailComponent)
  .config(MailboxConfig)
  .name;
