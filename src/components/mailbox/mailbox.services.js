/* global API_URL */

export const MailboxEmail = ($http) => {
  'ngInject';
  const actions = {};

  actions.inbox = project => {
    return $http({
      method: 'get',
      url: `${API_URL}/emails?filter[isDeleted]=false${(project ? `&filter[_project]=${project}` : '')}`
    }).then(response => {
      return response.data;
    });
  };

  actions.trash = () => {
    return $http({
      method: 'get',
      url: `${API_URL}/emails?filter[isDeleted]=true`
    }).then(response => {
      return response.data;
    });
  };

  actions.setRead = id => {
    return $http({
      method: 'put',
      data: {
        isRead: true
      },
      url: `${API_URL}/emails/${id}`
    }).then(response => {
      return response.data;
    });
  };

  actions.detail = id => {
    return $http({
      method: 'get',
      url: `${API_URL}/emails/${id}`
    }).then(response => {
      return response.data;
    });
  };

  actions.toTrash = id => {
    return $http({
      method: 'put',
      data: {
        isDeleted: true
      },
      url: `${API_URL}/emails/${id}`
    }).then(response => {
      return response.data;
    });
  };

  actions.remove = id => {
    return $http({
      method: 'delete',
      url: `${API_URL}/emails/${id}`
    }).then(response => {
      return response.data;
    });
  };

  return actions;
};

export const MailboxProject = ($http) => {
  'ngInject';
  const actions = {};

  actions.list = () => {
    return $http({
      method: 'get',
      url: `${API_URL}/projects`
    }).then(response => {
      return response.data;
    });
  };

  return actions;
};
