import angular from 'angular';

export class MailboxController {
  constructor(MailboxProject) {
    'ngInject';
    this.MailboxProject = MailboxProject;
    this.list();
  }

  list() {
    this.MailboxProject.list().then(items => this.items = items);
  }
}

export class MailboxListController {
  constructor(MailboxEmail, AuthService, $scope, $stateParams, $state, $q) {
    'ngInject';
    this.$q = $q;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.MailboxEmail = MailboxEmail;
    this.AuthService = AuthService;
    this.type = $scope.type;
    this.checkAll = false;
    this.isOwner = undefined;
    this.AuthService.getCurrentUser().then(user => {
      this.currentUser = {
        _user: {
          login: user.login
        }
      };
    });
    this.list();
  }

  toggleAll() {
    angular.forEach(this.items, item => {
      item.isSelected = this.checkAll;
    });
  }

  toggleOwner() {
    this.isOwner = !this.isOwner;
  }

  markRead() {
    const promises = this.checked.map((item) => {
      return this.MailboxEmail.setRead(item._id);
    }, this);

    this.$q.all(promises).then(() => {
      this.$state.transitionTo(this.$state.current, this.$stateParams, {
        reload: true,
        inherit: false,
        notify: true
      });
    });
  }

  list() {
    this.MailboxEmail[this.type](this.$stateParams.project).then(items => this.items = items);
  }

  remove() {
    const method = this.type === 'trash' ? 'remove' : 'toTrash';
    const promises = this.checked.map((item) => {
      return this.MailboxEmail[method](item._id);
    }, this);

    this.$q.all(promises).then(() => {
      this.$state.transitionTo(this.$state.current, this.$stateParams, {
        reload: true,
        inherit: false,
        notify: true
      });
    });
  }
}

export class MailboxDetailController {
  constructor($stateParams, $state, MailboxEmail) {
    'ngInject';
    this.MailboxEmail = MailboxEmail;
    this.$state = $state;
    this.detail($stateParams.id);
  }

  detail(id) {
    this.MailboxEmail.detail(id).then(item => this.item = item);
  }

  remove(item) {
    if (item.isDeleted) {
      this.MailboxEmail.remove(item._id).then(() => {
        this.$state.go('app.mailbox.trash');
      });
    } else {
      this.MailboxEmail.toTrash(item._id).then(() => {
        this.$state.go('app.mailbox.inbox');
      });
    }
  }
}
