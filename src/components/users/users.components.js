import UsersListTemplate from './templates/users-list.jade';
import {UsersListController} from './users.controllers';


export const UsersListComponent = () => {
  return {
    controller: UsersListController,
    controllerAs: 'ulc',
    template: UsersListTemplate,
    restrict: 'E'
  };
};
