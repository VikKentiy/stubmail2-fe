export const UsersConfig = ($stateProvider) => {
  'ngInject';
  $stateProvider
    .state('app.mailbox.users', {
      url: '^/users',
      template: '<users></users>'
    });
};
