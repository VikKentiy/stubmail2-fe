import LoginTemplate from './templates/auth.jade';
import LogoutTemplate from './templates/logout.jade';
import {
  AuthLoginController,
  AuthLogoutController
} from './auth.controllers';

export const LoginComponent = () => {
  return {
    template: LoginTemplate,
    controller: AuthLoginController,
    controllerAs: 'al',
    restrict: 'E',
    replace: false
  };
};

export const LogoutComponent = () => {
  return {
    template: LogoutTemplate,
    controller: AuthLogoutController,
    controllerAs: 'alogout',
    restrict: 'E',
    replace: true
  };
};
