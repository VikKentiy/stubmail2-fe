/* global API_URL */

export const AuthService = ($q, $timeout, $localStorage, $http) => {
  'ngInject';
  const actions = {};

  actions.login = (username, password) => {
    return $http({
      method: 'post',
      url: `${API_URL}/auth/login`,
      data: {
        login: username,
        password: password
      }
    }).then(response => {
      return response.data.token;
    });
  };

  actions.logout = () => {
    const defer = $q.defer();
    delete $localStorage.token;
    delete $localStorage.user;
    defer.resolve();
    return defer.promise;
  };

  actions.me = () => {
    return $http({
      method: 'get',
      url: `${API_URL}/users/me`
    }).then(response => {
      return response.data;
    });
  };

  actions.getCurrentUser = () => {
    const defer = $q.defer();
    defer.resolve($localStorage.user);
    return defer.promise;
  };

  actions.setCurrentUser = data => {
    const defer = $q.defer();
    if (!data) {
      return defer.reject('Current User is empty');
    }
    $localStorage.user = data;
    defer.resolve();
    return defer.promise;
  };

  actions.setToken = token => {
    const defer = $q.defer();
    if (!token) {
      return defer.reject('Token is empty');
    }
    $localStorage.token = token;
    defer.resolve();
    return defer.promise;
  };

  actions.getToken = () => {
    return $localStorage.token;
  };

  return actions;
};
