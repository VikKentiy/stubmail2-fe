export class AuthLoginController {
  constructor($state, AuthService) {
    'ngInject';
    this.$state = $state;
    this.AuthService = AuthService;
  }

  submit(isValid) {
    if (!isValid) {
      return false;
    }

    this.isError = false;
    this.AuthService.login(this.data.username, this.data.password).then(token => {
      return this.AuthService.setToken(token);
    }).then(() => {
      return this.AuthService.me();
    }).then((data) => {
      return this.AuthService.setCurrentUser(data);
    }).then(() => {
      this.$state.go('app.mailbox.inbox');
    }).catch(() => {
      this.isError = true;
      this.error = 'Wrong Username or Password';
    });
  }
}

export class AuthLogoutController {
  constructor($state, AuthService) {
    'ngInject';
    this.$state = $state;
    this.AuthService = AuthService;
  }

  logout($event) {
    $event.preventDefault();
    this.AuthService.logout().then(() => {
      this.$state.go('app.auth');
    });
  }
}
