import {
  AuthInterceptor
} from './auth.interceptors';

export const AuthConfig = ($httpProvider, $stateProvider) => {
  'ngInject';
  $httpProvider.interceptors.push(AuthInterceptor);
  $stateProvider
    .state('app.auth', {
      url: '/auth/signin',
      template: '<auth></auth>'
    });
};
