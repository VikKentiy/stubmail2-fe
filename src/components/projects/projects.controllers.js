import _ from 'lodash';

export class ProjectsListController {
  constructor(Project, User, $stateParams, $state, $q) {
    'ngInject';
    this.$q = $q;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.Project = Project;
    this.User = User;
    this.checkAll = false;
    this.list();
  }

  list() {
    this.Project.all().then(items => this.items = items);
  }


  findUsers(query) {
    return this.User.find(query);
  }

  add() {
    this.inserted = {
      title: ''
    };
    this.items.unshift(this.inserted);
  }

  save(data, id, $projectForm) {

    _.extend(data, {_id: id});
    var d = this.$q.defer();

    this.Project.save(data)
      .then(function(resp) {
        this.inserted._id = resp._id;
        d.resolve()
      }.bind(this))
      .catch(function(data){
        //Angular x-editable errors showing feature doesn't work properly
        // promise resolution with string argument doesn't trigger setError for given element
        //so, we passing $projectForm directive into save method to call setError directly
        if(data.status == 422) {
          $projectForm.$setError('title', 'project with the same name already exists');
        } else {
          $projectForm.$setError('title', 'Server responded with error');
        }
        d.resolve("error");
      });

    return d.promise;
  }

  tagAdded(user, project) {
    this.save(project, project._id);
  }

  tagRemoved(user, project) {
    this.save(project, project._id);
  }

  destroy(item) {
    this.Project.destroy(item._id).then(() => {
      this.$state.transitionTo(this.$state.current, this.$stateParams, {
        reload: true,
        inherit: false,
        notify: true
      });
    });
  }
}
