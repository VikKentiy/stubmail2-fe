export const ProjectsConfig = ($stateProvider) => {
  'ngInject';
  $stateProvider
    .state('app.mailbox.projects', {
      url: '^/projects',
      template: '<projects></projects>'
    });
};
