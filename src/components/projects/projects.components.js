import ProjectsListTemplate from './templates/projects-list.jade';
import {
  ProjectsListController
} from './projects.controllers';

export const ProjectsListComponent = () => {
  return {
    controller: ProjectsListController,
    controllerAs: 'plc',
    template: ProjectsListTemplate,
    restrict: 'E'
  };
};
