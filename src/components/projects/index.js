import angular from 'angular';
import 'angular-moment';
import 'ng-tags-input';

import 'ng-tags-input/build/ng-tags-input.css';
import 'ng-tags-input/build/ng-tags-input.bootstrap.css';

import {
  ProjectsListComponent
} from './projects.components';
import {
  Project
} from './projects.services';
import {
  ProjectsConfig
} from './projects.config';

export default angular
  .module('app.projects', ['ui.router', 'angularMoment', 'ngTagsInput'])
  .service('Project', Project)
  .directive('projects', ProjectsListComponent)
  .config(ProjectsConfig)
  .name;
