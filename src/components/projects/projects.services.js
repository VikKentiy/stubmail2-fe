/* global API_URL */

export const Project = ($http) => {
  'ngInject';
  const actions = {};

  actions.all = () => {
    return $http({
      method: 'get',
      url: `${API_URL}/projects`
    }).then(response => {
      return response.data;
    });
  };

  actions.save = data => {
    const url = `${API_URL}/projects` + (data._id ? `/${data._id}` : '');
    const method = data._id ? 'put' : 'post';

    return $http({
      method: method,
      url: url,
      data: data
    }).then(response => {
      return response.data;
    });
  };

  actions.destroy = id => {
    return $http({
      method: 'delete',
      url: `${API_URL}/projects/${id}`
    }).then(response => {
      return response.data;
    });
  };

  return actions;
};
