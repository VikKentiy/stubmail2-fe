import angular from 'angular';
import Mailbox from './mailbox';
import Auth from './auth';
import Users from './users';
import Projects from './projects';

export default angular
  .module('app.components', [Mailbox, Auth, Users, Projects])
  .name;
