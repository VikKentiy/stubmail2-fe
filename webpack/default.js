var webpack           = require('webpack');
var CleanPlugin       = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: ['./app.js']
  },

  output: {
    path: './build',
    filename: 'bundle-[hash:4].js'
  },

  module: {
    loaders: [
      {test: /\.js$/, loader: 'ng-annotate!babel', exclude: /node_modules/},
      {test: /\.scss/, loader: ExtractTextPlugin.extract('style', 'css!sass')},
      {test: /\.less/, loader: ExtractTextPlugin.extract('style', 'css!less?sourceMap')},
      {test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css')},
      {test: /\.woff2?(.*)?$/, loader: 'file?name=fonts/[name].[ext]' },
      {test: /\.(ttf|eot|svg)(.*)?$/, loader: 'file?name=fonts/[name].[ext]' },
      {test: /\.jade$/, loader: 'jade'},
      {test: /\.html$/, loader: 'html'},
      {test: /\.png|\.jpg|\.gif$/, loader: 'file?name=img/[path][name].[ext]'}
    ]
  },

  plugins: [
    new CleanPlugin(['build']),
    new webpack.optimize.DedupePlugin(),
    new ExtractTextPlugin('[name].css'),
    new HtmlWebpackPlugin({
      title: 'StubMail',
      template: './src/index.html',
      inject: 'body'
    }),
    new webpack.DefinePlugin({
      API_URL: JSON.stringify('http://localhost')
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/) // fix for moment and locales
  ]
};
