var webpack    = require('webpack');
var _          = require('lodash');

var env        = process.env.NODE_ENV;
module.exports = _.merge({context: __dirname + '/src'}, require('./webpack/default'), env && require(`./webpack/${env}`));
